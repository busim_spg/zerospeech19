#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 15:23:23 2019

@author: yusuf
"""

import os
import time
import argparse
import copy
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
# from torchvision import transforms
from torch import optim
from torch.nn.utils import clip_grad_norm_
import nn_misc


class forwardRNN(nn.Module):
    def __init__(self, document_dim,
                 projection_dim=None,
                 hidden_dim=128,
                 num_recurrent_layers=1,
                 bidirectional=False,
                 dropout=0):
        super(forwardRNN, self).__init__()
        if isinstance(document_dim, str):  # Load config from file
            document_dim, hidden_dim, projection_dim, \
                num_recurrent_layers, bidirectional = loadstr(document_dim)
            bidirectional = bool(bidirectional)
        self.hidden_dim = hidden_dim
        assert projection_dim is not None
        self.projection_dim = projection_dim
        self.document_dim = document_dim
        self.num_recurrent_layers = num_recurrent_layers
        self.bidirectional = bidirectional
        if self.bidirectional:
            self.num_directions = 2
        else:
            self.num_directions = 1
        self.gru = nn.GRU(document_dim, hidden_dim,
                          num_layers=self.num_recurrent_layers,
                          bidirectional=self.bidirectional,
                          dropout=dropout)
        self.W = nn.Linear(self.num_directions*hidden_dim, projection_dim)
        self.dimstr = '{},{},{},{},{}'.format(document_dim, hidden_dim,
                                              projection_dim,
                                              num_recurrent_layers,
                                              int(bidirectional))

    def forward(self, x, hidden, return_hidden=True):
        self.gru.flatten_parameters()
        x, hidden = self.gru(x, hidden)
        hidden = hidden.detach()
        if return_hidden:
            return x, hidden
        else:
            return x

    def eval_sequence(self, sequence_x):
        b, a, c = sequence_x.size()
        hidden = self.initHidden(a)
        output = self.forward(sequence_x, hidden, return_hidden=False)
        return self.W(output)
        # return nn.LogSoftmax(self.W(output), dim=-1)
        # F.softmax(self.W(output), dim=-1)

    def initHidden(self, batch_size=1):
        return torch.zeros(self.num_directions*self.num_recurrent_layers,
                           batch_size,
                           self.hidden_dim,
                           device=next(self.gru.parameters()).device)

    def save(self, outdir):
        chk_mkdir(outdir)
        with open(os.path.join(outdir, 'nnet.txt'), 'w') as _outfile:
            _outfile.write(self.dimstr)
        torch.save(self.state_dict(), os.path.join(outdir, 'nnet.mdl'))

    @classmethod
    def load_from_dir(cls, nnetdir, map_location=None):
        net = cls(os.path.join(nnetdir, 'nnet.txt'))
        net.load_state_dict(torch.load(os.path.join(nnetdir, 'nnet.mdl'),
                                       map_location=map_location))
        return net


def realign(ali, th=0):
    clusters = np.array([int(i//(th+1)) for i in range(ali.max() + 1)])
    y = clusters[ali]
    y = y.astype('long')
    return y


class ShortFeatDataset(Dataset):
    def __init__(self, data,
                 alignment,
                 keys,
                 offsets,
                 data_slice=None, frame_length=0.01, N=1,
                 transform=None,
                 **transform_args):
        self.data = data
        self.keys = keys
        self.alignment = alignment
        self.offsets = offsets
        if data_slice is not None:
            self.data = self.data[data_slice]
            self.alignment = self.alignment[data_slice]
        self.N = int(N//frame_length)
        self.frame_length = frame_length
        self.indices = range(0, len(self.data), self.N)
        self.K = data.shape[1]
        self.transform = transform
        self.transform_args = transform_args

    def __len__(self):
        return len(self.indices)

    def __getitem__(self, index):
        beg = self.indices[index]
        x = self.data[beg:beg+self.N]
        a = self.alignment[beg:beg+self.N]
        if len(a) < self.N:  # At the end of the document
            length_to_add = self.N - len(a)
            x = self.data[beg - length_to_add:beg + self.N - length_to_add]
            a = self.alignment[beg-length_to_add:beg+self.N - length_to_add]
        if self.transform is not None:
            x, a = self.transform([x, a], **self.transform_args)
        return x, a


def loadstr(filename, sep=',', dtype=int):
    phrase = open(filename).read().split(sep)
    try:
        diter = iter(dtype)
        assert len(dtype) == len(phrase)
        return [dtype[i](x) for i, x in enumerate(diter)]
    except TypeError:
        return [dtype(x) for x in phrase]


def chk_mkdir(dirname):
    if not os.path.isdir(dirname):
        os.makedirs(dirname)


def get_device(no_cuda):
    if no_cuda:
        dev_kind = "cpu"
    elif torch.cuda.is_available():
        dev_kind = "cuda:0"
    else:
        print("Warning: no_cuda is set to false, but no GPU is found"
              + "CPU will be used.")
        dev_kind = "cpu"
    return torch.device(dev_kind)


def since(tbeg):
    return time.time() - tbeg


def train_model(models, criterion, optimizer, data_loaders,
                device, outdir,
                num_epochs=40, verbose=True,
                use_early_stopping=True,
                num_epochs_early_stopping=30,
                delta_early_stopping=1e-4,
                learning_rate_lower_bound=1e-6,
                learning_rate_scale=0.5,
                num_epochs_reduce_lr=12,
                num_epochs_cooldown=8,
                use_model_checkpoint=True,
                model_checkpoint_period=8,
                start_epoch=0,
                gradient_clip=None,
                start_lr_scale=1,
                start_lr_num_epochs=10,
                History=[]):

    dataset_sizes = {x: len(data_loaders[x].dataset)
                     for x in ['train', 'test']}
    batch_sizes = {x: data_loaders[x].batch_size for x in ['train', 'test']}
    best_model_wts = [copy.deepcopy(model.state_dict()) for model in models]
    best_loss = 2000
    Losses = {phase: [] for phase in ['train', 'test']}
    cooldown = 0
    frnn = models[0]
    lrs = [optimizer.param_groups[0]['lr']]
    for epoch in range(start_epoch, num_epochs):
        if epoch <= start_lr_num_epochs:
            lrs = [start_lr_scale*lr for lr in lrs]
        optimizer.param_groups[0]['lr'] = lrs[0]
        print('Epoch {}/{} - lr={}'.format(
                epoch+1, num_epochs, lrs[0]))
        epoch_start = time.time()
        for phase in ['train', 'test']:
            if phase == 'train':
                frnn.train()
            else:
                frnn.eval()
            running_loss = 0
            running_accuracy = 0.
            running_denom = 0.
            for batch_no, batch_data in enumerate(data_loaders[phase]):
                feat, labels = batch_data
                feat = torch.transpose(feat, 1, 0)

                feat = feat.to(device)
                labels = labels.to(device)
                optimizer.zero_grad()
                with torch.set_grad_enabled(phase == 'train'):
                    z = frnn.eval_sequence(feat)
                    z = z.transpose(1, 2)
                    z = z.transpose(0, 2)
                    loss = criterion(z, labels)
                if phase == 'train':
                    loss.backward()
                    if gradient_clip:
                        for model in models:
                            clip_grad_norm_(model.parameters(),
                                            gradient_clip)
                    optimizer.step()

                bb = torch.ones_like(labels).float()

                pred_class = torch.argmax(z, dim=1)
                accuracy = (pred_class == labels).float()
                denom = torch.ones_like(accuracy)

                running_loss += loss * labels.size(0)
                running_accuracy += (accuracy * bb).sum()
                running_denom += (denom * bb).sum()
                phase_elapse = since(epoch_start)
                eta = int(phase_elapse
                          * (dataset_sizes[phase]//batch_sizes[phase]
                              - batch_no - 1)
                          / (batch_no + 1))
                if verbose:
                    print('\r\t{} batch: {}/{} batches - ETA: {}s'.format(
                            phase.title(),
                            batch_no+1,
                            dataset_sizes[phase]//batch_sizes[phase]+1,
                            eta), end='')

            epoch_loss = running_loss/dataset_sizes[phase]
            epoch_accuracy = running_accuracy/running_denom
            print(" - loss: {:.4f} - accuracy: {:.4f},".format(
                    epoch_loss.item(),
                    epoch_accuracy))
            Losses[phase].append([float(epoch_loss)])
            if phase == 'test':
                if epoch_loss < best_loss:
                    best_loss = epoch_loss
                    best_model_wts = [copy.deepcopy(model.state_dict())
                                      for model in models]
                    models[0].save(os.path.join(outdir, 'frnn'))
                    torch.save(optimizer.state_dict(), os.path.join(outdir,
                               'optimizer.state'))
                History.append(epoch_loss)
        if use_model_checkpoint and epoch % model_checkpoint_period == 0:
            models[0].save(os.path.join(
                    outdir,
                    'checkpoint_epoch_{}'.format(epoch),
                    'frnn'))
            torch.save(optimizer.state_dict(),
                       os.path.join(
                               outdir,
                               'checkpoint_epoch_{}'.format(epoch),
                               'optimizer.state'))
        rlop = nn_misc.reduce_lr_on_plateau
        lrs = [rlop(History, lr, cooldown,
                    num_epochs_reduce_lr, mode='min',
                    difference=delta_early_stopping,
                    lr_scale=learning_rate_scale,
                    lr_min=learning_rate_lower_bound,
                    cool_down_patience=num_epochs_cooldown)
               for lr in lrs]
        cooldown = lrs[0][1]
        lrs = [lr[0] for lr in lrs]
        print('\tTime: {}s'.format(int(since(epoch_start))))
        if use_early_stopping and \
            nn_misc.StopEarly(History, patience=num_epochs_early_stopping,
                              mode="min", difference=delta_early_stopping):
            print('Stopping Early.')
            break
        if epoch <= start_lr_num_epochs:
            lrs = [lr/start_lr_scale for lr in lrs]
    for i in range(len(models)):
        models[i].load_state_dict(best_model_wts[i])
    return models, Losses


def main():
    parser = argparse.ArgumentParser(description='RNN for predicting '
                                     'acoustic units')
    parser.add_argument('datadir', help='The training features directory. '
                        'Should contain: '
                        '    data.npy: A matrix of the features'
                        '    ali_data.npy: A matrix of labels'
                        '    offsets_data.npy: A matrix utterance boundaries'
                        '    keys_data.txt: A list of utterance names')
    parser.add_argument('output_directory', help='Model output directory')

    parser.add_argument('--no-cuda', action='store_true',
                        help='If specified, turns off gpu usage.')
    parser.add_argument('--ali-neighborhood', '--an', type=int, default=4,
                        help='SOM kinship neighborhood. Units within the same '
                        'neighborhood are pooled into the same unit')

    parser.add_argument('--batch-size', '-b', default=64, type=int,
                        help='Batch size')
    parser.add_argument('--hidden-dim', '--hd',
                        type=int, default=24,
                        help='Dimension of the RNN hidden layers')
    parser.add_argument('--num-recurrent-layers', '--nrl',
                        type=int, default=2,
                        help='Number of hidden layers in the RNN')
    parser.add_argument('--bidirectional', '--bd',
                        action='store_true',
                        help='Use bidirectional RNN')
    parser.add_argument('--dropout', type=float, default=0.4,
                        help='Amount of dropout to use in the RNN')
    parser.add_argument('--validation-split', '--spl', type=float, default=0.2,
                        help='Ratio of data to use of validation')
    parser.add_argument('--max-len', '--uml', type=float, default=1,
                        help='Maximum length a sample (in seconds) '
                        'for RNN training')

    parser.add_argument('--num-epochs', default=500, type=int,
                        help='Maximum number of training epochs')
    parser.add_argument('--no-early-stopping', '--es', action='store_true',
                        help="Don't use earlys stopping criterion")
    parser.add_argument('--num-epochs-early-stopping', '--nes',
                        type=int, default=15,
                        help='Number of epochs to wait for '
                        'before early stopping')
    parser.add_argument('--delta-early-stopping', '--des',
                        type=float, default=0.0001,
                        help='The minimum difference for '
                        'early stopping deference')
    parser.add_argument('--no-model-checkpoint', '--mcu',
                        action='store_true',
                        help="Don't store intermediate models")
    parser.add_argument('--model-checkpoint-period', '--mcp',
                        type=int, default=8,
                        help='Number of epochs after which to '
                        'store intermediate models')

    parser.add_argument('--initial-learning-rate', '--lr',
                        type=float, default=1e-3,
                        help='Optimization learning rate for document RNN')
    parser.add_argument('--start-lr-scale', '--sls',
                        type=float, default=1,
                        help='Scale to use on the learning rate for'
                        'earlier epochs')
    parser.add_argument('--start-lr-num-epochs', '--slne',
                        type=int, default=10,
                        help='Number of epochs for which to use the '
                        'scaled learning rate')

    parser.add_argument('--learning-rate-lower-bound', '--lr-lower',
                        type=float, default=1e-6,
                        help='Learning rate lower bound')
    parser.add_argument('--learning-rate-scale', '--lr-scale',
                        type=float, default=0.5,
                        help='Learning rate plateau reduction scale')
    parser.add_argument('--num-epochs-reduce-lr', '--nlrr',
                        type=int, default=6,
                        help='Number of epochs to wait before '
                        'reducing the learning rate')
    parser.add_argument('--num-epochs-cooldown', '--nec', type=int, default=3,
                        help='Number of epochs to cooldown '
                        'before tracking plateaus')
    parser.add_argument('--start-epoch', '--se', type=int, default=0,
                        help='The epoch from which to start training. '
                        'Only set to a nonzero value if the checkpoint '
                        'directory exists')
    parser.add_argument('--gradient-clip', '--gc', type=float,
                        help='Gradient clipping value')

    args = parser.parse_args()
    datadir = args.datadir
    outdir = args.output_directory
    ali_neighborhood = args.ali_neighborhood
    no_cuda = args.no_cuda

    batch_size = args.batch_size
    val_split = args.validation_split
    num_epochs = args.num_epochs
    lr = args.initial_learning_rate
    gradient_clip = args.gradient_clip

    hidden_dim = args.hidden_dim
    num_recurrent_layers = args.num_recurrent_layers
    bidirectional = args.bidirectional
    dropout = args.dropout

    use_early_stopping = not args.no_early_stopping
    num_epochs_early_stopping = args.num_epochs_early_stopping
    delta_early_stopping = args.delta_early_stopping
    learning_rate_lower_bound = args.learning_rate_lower_bound
    learning_rate_scale = args.learning_rate_scale
    start_lr_scale = args.start_lr_scale
    start_lr_num_epochs = args.start_lr_num_epochs
    num_epochs_reduce_lr = args.num_epochs_reduce_lr
    num_epochs_cooldown = args.num_epochs_cooldown
    use_model_checkpoint = not args.no_model_checkpoint
    model_checkpoint_period = args.model_checkpoint_period
    start_epoch = args.start_epoch

    device = get_device(no_cuda)
    data = np.load(os.path.join(datadir, 'data.npy')).astype('float32')
    labels = np.load(os.path.join(datadir, 'ali_data.npy')).astype('long')
    print(labels.shape)
    print(labels.mean())
    if ali_neighborhood:
        labels = realign(labels, ali_neighborhood)
        print(labels.shape)
        print(labels.mean())
    projection_dim = max(labels) + 1
    keys = {line.strip(): i for i, line
            in enumerate(open(os.path.join(datadir, 'keys_data.txt')))}
    offsets = np.load(os.path.join(datadir, 'offsets_data.npy'))
    validation_length = int(val_split * len(labels))
    train_slice = slice(validation_length, len(labels))
    val_slice = slice(0, validation_length)
    # data = np.array_split(data, offsets)

    dataset_class = ShortFeatDataset
    train_dataset = dataset_class(data, labels, keys, offsets,
                                  data_slice=train_slice)
    # transform=realign,
    # neighborhood=ali_neighborhood)
    val_dataset = dataset_class(data, labels, keys, offsets,
                                data_slice=val_slice)
    # transform=realign,
    # neighboorhood=ali_neighborhood)

    train_dataloader = DataLoader(train_dataset, batch_size=batch_size,
                                  shuffle=True, num_workers=5,
                                  pin_memory=True)
    val_dataloader = DataLoader(val_dataset, batch_size=2*batch_size,
                                num_workers=5, pin_memory=True)

    dataloaders = {'train': train_dataloader,
                   'test': val_dataloader}
    frnn = forwardRNN(data.shape[1],
                      projection_dim=projection_dim,
                      num_recurrent_layers=num_recurrent_layers,
                      hidden_dim=hidden_dim,
                      bidirectional=bidirectional,
                      dropout=dropout)
    optimizer = optim.Adam(frnn.parameters(), lr)
    if start_epoch:
        frnn = forwardRNN.load_from_dir(
                os.path.join(outdir,
                             'checkpoint_epoch_{}'.format(start_epoch),
                             'frnn')
                )
        frnn.to(device)
        if os.path.isfile(os.path.join(
                outdir,
                'checkpoint_epoch_{}'.format(start_epoch),
                'optimizer.state')):
            optimizer = optim.Adam(frnn.parameters(), lr)
            optimizer.load_state_dict(torch.load(
                    os.path.join(outdir,
                                 'checkpoint_epoch_{}'.format(start_epoch),
                                 'optimizer.state')))
    frnn.to(device)
    chk_mkdir(outdir)
    criterion = nn.CrossEntropyLoss()
    mdls = [frnn]
    mdls, L = train_model(mdls, criterion, optimizer,
                          dataloaders, device, outdir,
                          num_epochs=num_epochs,
                          use_early_stopping=use_early_stopping,
                          num_epochs_early_stopping=num_epochs_early_stopping,
                          delta_early_stopping=delta_early_stopping,
                          learning_rate_lower_bound=learning_rate_lower_bound,
                          learning_rate_scale=learning_rate_scale,
                          num_epochs_reduce_lr=num_epochs_reduce_lr,
                          num_epochs_cooldown=num_epochs_cooldown,
                          use_model_checkpoint=use_model_checkpoint,
                          model_checkpoint_period=model_checkpoint_period,
                          start_lr_scale=start_lr_scale,
                          start_lr_num_epochs=start_lr_num_epochs,
                          start_epoch=start_epoch,
                          gradient_clip=gradient_clip)

    frnn = mdls[0]
    frnn.save(os.path.join(outdir, 'frnn'))
    torch.save(optimizer.state_dict(),
               os.path.join(
                       outdir,
                       'optimizer.state'
                       )
               )
    torch.save(optimizer.state_dict(), os.path.join(outdir, 'optimizer.state'))


if __name__ == '__main__':
    x = main()
