# -*- coding: utf-8 -*-
"""
Created on Fri Feb  1 11:17:09 2019

@author: USER
"""

import os
import time
import argparse
import pickle
import numpy as np
from sklearn.decomposition import PCA
from nn_misc import StopEarly, reduce_lr_on_plateau as rlop


class Opts:
    def __init__(self, list_opts, convert=None, *args, **kind_args):
        if convert is None:
            self.opts = [list_opts]
        else:
            self.opts = convert(list_opts, *args, **kind_args)

    def check(self, inp):
        if inp not in self.opts:
            raise ValueError("{} options allowed. {} provided".format(
                    self.opts, inp))


class MapNN:
    def __init__(self, num_classes, init_params='rand_sample',
                 covariance_type='diag', max_iter=100, lr=0.01, eps=1e-5,
                 num_iter_means=10, n_init=10, max_length=200, t_lambda=1e7,
                 u_lambda=1e7, min_precision=0.1, max_precision=2,
                 batch_length=128, converge_eps=1e-1, featdim=None):
        self.init_opts = Opts('rand_sample random global ones zeros',
                              to_inds, str, " ")
        self.covariance_opts = Opts('identity', to_inds, str, " ")
        self.init_opts.check(init_params)
        self.covariance_opts.check(covariance_type)

        self.num_classes = num_classes
        self.init_params = init_params
        self.covariance_type = covariance_type
        self.max_iter = max_iter
        self.num_iter_means = num_iter_means
        self.lr = lr
        self.eps = eps
        self.n_init = n_init
        self.featdim = featdim
        self.means = None
        self.precision_matrices = None
        self.t_lambda = t_lambda
        self.u_lambda = u_lambda
        self.batch_length = batch_length
        self.N = np.zeros(num_classes)
        self.cov_update = None
        self.neighborhood_mat = None
        self.loss = [0, 0]
        self.converge_eps = converge_eps
        self.History = []
        assert self.batch_length <= max_length
        self.max_length = max_length
        self.max_precision = max_precision
        self.min_precision = min_precision
        self.set_D()
        self.UD = self.set_D(self.num_classes, self.u_lambda, True)

    def set_neighborhood(self, n):
        assert n > 0 and n < self.num_classes
        n_mat = np.eye(self.num_classes, self.num_classes - n + 1)
        for i in range(1, n):
            n_mat += np.roll(np.eye(self.num_classes,
                                    self.num_classes - n + 1), i, axis=0)
        self.neighborhood_mat = n_mat

    def set_D(self, max_length=None, lamb=None, ret=False):
        if max_length is None:
            max_length = self.max_length
        if lamb is None:
            lamb = self.t_lambda
        D = np.zeros((max_length, max_length), dtype='float32')
        for i in range(max_length):
            for j in range(max_length):
                D[i, j] = np.exp(-lamb*(i - j)**2)
        if ret:
            return D
        else:
            self.D = D

    def init_means(self, shape, data=None):
        if self.init_params == 'zeros':
            self.means = np.zeros(shape)
        elif self.init_params == 'ones':
            self.means = np.ones(shape)
        elif self.init_params == 'random':
            self.means = np.random.randn(*shape)
        elif self.init_params == 'global':
            assert isinstance(data, np.ndarray)
            mean = data.reshape(-1, data.shape[-1]).mean(axis=0)
            self.means = np.tile(mean, [self.num_classes, 1])
        elif self.init_params == 'rand_sample':
            assert isinstance(data, np.ndarray)
            data_ = data.reshape(-1, data.shape[-1])
            std = 0
            for i in range(self.n_init):
                mean = np.random.choice(data_.shape[0], self.num_classes)
                mean = data_[mean]
                cnum = -mat_euclidean(mean, mean).sum()
                if cnum >= std:
                    std = cnum
                    mean_ = mean.copy()
            self.means = mean_

    def init_cov(self, shape, data=None):
        self.precision_matrices = np.ones(shape)
        if data is not None:
            data_ = np.reshape(data, [-1, data.shape[-1]])
            if self.covariance_type == 'identity':
                self.precision_matrices = np.ones(shape)
            if self.covariance_type == 'diag':
                if data is None:
                    self.precision_matrices = np.ones(shape)
                else:
                    cov_ = data_.var(axis=0)
                    self.precision_matrices = np.tile(cov_,
                                                      [self.num_classes, 1])
            if self.covariance_type == 'full':
                cov_ = np.cov(data_.T)
                self.precision_matrices = np.tile(np.linalg.inv(cov_),
                                                  [self.num_classes, 1, 1])

    def forward(self, data, verbose=False):
        n, d = data.shape
        assert d == self.featdim
        if n < self.D.shape[0]:
            D = self.D[0:n, 0:n]
        else:
            D = self.D
        if self.covariance_type != 'full':
            y = mat_euclidean(data, self.means, self.precision_matrices ** 2)
        # print(D)
        y = y.T.dot(D).T  # + y
        self.loss[0] += y.mean()
        self.loss[1] += 1
        if verbose:
            print(y.sum())
        return y

    def predict(self, data, verbose=False):
        y = self.forward(data, verbose=verbose)
        if self.neighborhood_mat is not None:
            y = np.matmul(y, self.neighborhood_mat)
        return y.argmax(axis=-1)

    def predict_dense(self, data, verbose=False):
        y = self.predict(data, verbose)
        y = self.means[y]
        return y

    def predict_one_hot(self, data, realign_neighborhood=0, verbose=False):
        y = self.predict(data, verbose)
        if realign_neighborhood:
            y = realign(y, realign_neighborhood)
        y = to_categorical(y, self.num_classes)
        return y

    def predict_proba(self, data, temperature=0.1, verbose=False):
        y = self.forward(data, verbose)
        y = softmax(y, axis=1, temperature=temperature)
        return y

    def train_batch(self, data, verbose=False):
        if self.featdim is None:
            self.featdim = data.shape[-1]
        else:
            assert self.featdim == data.shape[-1]
        if self.means is None:
            self.init_means((self.num_classes, self.featdim), data)
        if self.precision_matrices is None:
            self.init_cov((self.num_classes, self.featdim), data)
        if self.precision_matrices is None:
            self.init_cov(self.means.shape, data)
        vec_cats = self.predict(data, verbose=verbose)
        mean_update = np.zeros_like(self.means)
        mat_cats = to_categorical(vec_cats, self.num_classes)
        mean_update = mat_cats.T.dot(data)
        mean_update = self.UD.dot(mean_update)
        counts = np.linalg.norm(mat_cats, 1, axis=0)
        counts = self.UD.dot(counts)
        self.means -= self.lr * (counts[:, np.newaxis]
                                 * self.means - mean_update)

    def fit(self, x, verbose=False):
        if not isinstance(x, list):
            s1 = [_ for _ in range(0, x.shape[0], self.batch_length)][1:]
            s2 = [_ for _ in range(self.batch_length//2,
                                   x.shape[0],
                                   self.batch_length)]
            x_split = np.array_split(x, s1) + np.array_split(x, s2)
        else:
            x_split = x
        data = np.concatenate(x_split)
        if self.featdim is None:
            self.featdim = data.shape[-1]
        else:
            assert self.featdim == data.shape[-1]
        if self.means is None:
            self.init_means((self.num_classes, self.featdim),
                            np.concatenate(x_split))
        converged = False
        i = 0
        cooldown = 0
        while not converged:
            i += 1
            t1 = time.time()
            m1 = self.means.copy()
            np.random.shuffle(x_split)
            for j, x_ in enumerate(x_split):
                self.train_batch(x_, verbose=False)
            self.clear_accs()
            red = abs(self.means - m1).mean()
            self.History.append(red)
            if verbose:
                print("Epoch: {} - time: {:.2f}s - lr: {} - loss:".format(
                        i,
                        time.time() - t1,
                        self.lr),
                      end=' ')
                print("{:.4f}".format(red))
            self.clear_loss(False)
            if i >= self.max_iter or StopEarly(self.History, mode="max",
                                               difference=0,
                                               abs_comp=-self.converge_eps):
                converged = True
            lr, cooldown = rlop(self.History, self.lr, cooldown,
                                patience=5, mode='min',
                                difference=0,
                                lr_scale=0.5,
                                lr_min=1e-5,
                                cool_down_patience=3,
                                abs_comp=3*self.converge_eps)
            self.lr = lr

    def clear_loss(self, verbose=False):
        if verbose:
            try:
                print(self.loss[0]/self.loss[1])
            except ZeroDivisionError:
                print(0)
        self.loss = [0, 0]


def to_categorical(vec_cats, max_val=None):
    if max_val is None:
        max_val = max(vec_cats) + 1
    mat_cats = np.eye(max_val)
    mat_cats = mat_cats[vec_cats]
    return mat_cats


def mat_euclidean(x, mu, precisions=None):
    if precisions is None:
        precisions = np.ones_like(mu)
    a = (x*x).dot(1/(precisions.T))
    b = (mu*mu/precisions).sum(axis=-1)
    D = -2*x.dot(mu.T/precisions.T)
    return -(D + b + a) + np.log(precisions).sum()


def softmax(x_, axis=0, temperature=0.1, eps=1e-7):
    x = x_ * temperature
    x = x - x.max(axis=axis)[:, np.newaxis]
    num = np.exp(x)
    denom = num.sum(axis=axis)
    denom = np.where(denom >= eps, denom, eps)
    if num.shape[-1] != denom.shape[-1]:
        denom = denom[:, np.newaxis]
    y = num/denom
    return y


def to_inds(str_inds, kind=int, sep=":"):
    return [kind(x) for x in str_inds.split(sep)]


def splice(x, indices):
    ld = x.shape[1]
    X = []
    for i in indices:
        if i < 0:
            x_i = np.pad(x, [[0, 0], [-i, 0]], mode='edge')[:, 0:ld]
        else:
            x_i = np.pad(x, [[0, 0], [0, i]], mode='edge')[:, -ld:]
        X.append(x_i)
    return np.concatenate(X, axis=1)


def chk_mkdir(dirname):
    if not os.path.isdir(dirname):
        if os.path.isfile(dirname):
            print('Specified output directory "{}" is a file'.format(dirname))
            exit(1)
        os.makedirs(dirname)


def load(filename):
    with open(filename, 'rb') as _filename:
        return pickle.load(_filename)


def save(obj, filename):
    with open(filename, 'wb') as _filename:
        pickle.dump(obj, _filename)


def create(num_classes, *args, **kwargs):
    return MapNN(num_classes, *args, **kwargs)


def fail_with(error):
    print(error)
    exit(1)


def realign(ali, th=0):
    l_clusters = []
    i = 1
    beg = 0
    ended = False
    while not ended:
        if abs(ali[i] - ali[beg]) > th:
            l_clusters.append((beg, i))
            beg = i
        i += 1
        if i == len(ali):
            ended = True
            if beg + 1 < i:
                l_clusters.append((beg, i+1))
    y = np.zeros_like(ali)
    for a, b in l_clusters:
        y[a:b] = ali[a:b].mean()
    y = y.astype(int)
    return y


def main():
    parser = argparse.ArgumentParser(description='Self-organizing map for '
                                     'discovering acoustic units')
    parser.add_argument('featdir', help='Input features directory. '
                        'Should have a feature matrix file called data.npy')
    parser.add_argument('modeldir', help='Directory containing the model')
    parser.add_argument('--posterior-feat-dir', '-d',
                        help='Directory to store posteriorgrams (optional)')
    parser.add_argument('--no-train', action='store_true',
                        help="Don't train model, use supplied one")
    parser.add_argument('--num-classes', '-n', type=int, default=128,
                        help='Number of classes in the mixture')
    parser.add_argument('--covariance-type', '-c', default='identity',
                        help='Type of covariance matrix '
                        '(identity or diag)')
    parser.add_argument('--pca-dim', '-p', type=int, default=20,
                        help='Dimensionality of PCA output')
    parser.add_argument('--time-splice', '-t',
                        type=to_inds, default="-2:-1:0:1:2",
                        help='Temporal context to consider')
    parser.add_argument('--transform',
                        help='External transform as .npy file.')
    parser.add_argument('--use-pca', action='store_true',
                        help='Skip pca step')
    parser.add_argument('--post-type', '--pt', default='one_best',
                        help='Kind of posterior features to store. One of: '
                        '(one_best, one_hot, softmax, means)')
    parser.add_argument('--post-neighborhood', '--pn', type=int,
                        help='Unit neighborhood around which to average in '
                        'posterior computations')
    parser.add_argument('--softmax-temperature', type=float, default=0.01,
                        help='Temperature to use for posterior computation '
                        'with softmax')
    parser.add_argument('--realign-neighborhood', '--rn', type=int,
                        help='Unit neighborhood around which to pool '
                        'in alignment')
    parser.add_argument('--num-iter', '-i', type=int, default=25,
                        help='Number of training iterations')
    parser.add_argument('--num-iter-means', '--nm', type=int, default=10,
                        help='Number of training iterations in which only '
                        'means are updated.')
    parser.add_argument('--num-init', '--ni', type=int, default=1,
                        help='Number of initializations to do. '
                        'The best one is kept')
    parser.add_argument('--init-params', default='rand_sample',
                        help='How to initialize the class means: '
                        '(rand_sample, random, global, ones or zeros)')
    parser.add_argument('--lr', type=float, default=1e-2,
                        help='Learning rate')
    parser.add_argument('--max-length', '--ml', type=int, default=1024,
                        help='Maximum length of sequence informaton')
    parser.add_argument('--t-lambda', '--tl', type=float, default=0.5,
                        help='Temporal neighborhood narrowness')
    parser.add_argument('--u-lambda', '--ul', type=float, default=0.1,
                        help='Unit neighborhood narrowness')
    parser.add_argument('--batch-length', '--bl', type=int, default=32,
                        help='Actual length of each sequence')
    parser.add_argument('--converge-eps', type=float, default=1e-1,
                        help='Minimum average change in centers')

    args = parser.parse_args()
    featdir = args.featdir
    modeldir = args.modeldir
    posterior_feat_dir = args.posterior_feat_dir
    no_train = args.no_train
    num_classes = args.num_classes
    num_iter = args.num_iter
    num_iter_means = args.num_iter_means
    num_init = args.num_init
    time_splice = args.time_splice
    transform = args.transform
    no_pca = not args.use_pca
    pca_dim = args.pca_dim
    init_params = args.init_params
    covariance_type = args.covariance_type
    lr = args.lr
    max_length = args.max_length
    t_lambda = args.t_lambda
    u_lambda = args.u_lambda
    batch_length = args.batch_length
    converge_eps = args.converge_eps

    post_type = args.post_type
    post_neighborhood = args.post_neighborhood
    softmax_temperature = args.softmax_temperature
    realign_neighborhood = args.realign_neighborhood
    post_opts = Opts('one_best one_hot softmax means', to_inds, str, " ")
    post_opts.check(post_type)

    chk_mkdir(modeldir)
    modelfile = os.path.join(modeldir, 'nnet.mdl')
    pcafile = os.path.join(modeldir, 'pca.mdl')
    feats = np.load(
            os.path.join(featdir, 'data.npy')
            ).astype('float32')

    if transform:
        transform_mat = np.load(transform)
        feats = feats.dot(transform_mat)
    offsets = np.load(
            os.path.join(featdir, 'offsets_data.npy')
            )
    utterances = {i: line.strip() for i, line
                  in enumerate(open(os.path.join(featdir, 'keys_data.txt')))}

    if os.path.isfile(modelfile):
        model = load(modelfile)
        pca = load(pcafile)
    else:
        if no_train:
            fail_with("Error: No saved model and no_train specified")
        model = create(num_classes, init_params=init_params,
                       covariance_type=covariance_type, max_iter=num_iter,
                       lr=lr, n_init=num_init, max_length=max_length,
                       t_lambda=t_lambda, u_lambda=u_lambda,
                       num_iter_means=num_iter_means,
                       batch_length=batch_length, converge_eps=converge_eps)
        pca = PCA(n_components=pca_dim)
    feats = splice(feats, time_splice)
    if not no_pca:
        assert pca.n_components <= min(feats.shape)
    if not no_train:
        if not no_pca:
            pca.fit(feats)
            feats = pca.transform(feats)
        featdim = feats.shape[-1]
        model.init_means([num_classes, featdim], feats)
        model.init_cov([num_classes, featdim], feats)
        model.fit(feats, verbose=True)
    elif not no_pca:
        feats = pca.transform(feats)
    save(model, modelfile)
    save(pca, pcafile)
    data = np.array_split(feats, offsets)
    max_len = max([len(_) for _ in data])
    if model.max_length <= max_len:
        model.max_length = max_len + 1
        model.set_D()
    if posterior_feat_dir:
        chk_mkdir(posterior_feat_dir)
        if post_neighborhood:
            model.set_neighborhood(post_neighborhood)
        for i, d in enumerate(data):
            utt = os.path.join(posterior_feat_dir, utterances[i])
            if post_type == 'one_best':
                output = model.predict(d)
                if realign_neighborhood:
                    output = realign(output, realign_neighborhood)
                np.savetxt(utt, output.astype(int), fmt='%d')
            elif post_type == 'one_hot':
                post = model.predict_one_hot(d,
                                             realign_neighborhood).astype(int)
                np.savetxt(utt, post, fmt='%d')
            elif post_type == 'softmax':
                post = model.predict_proba(d, temperature=softmax_temperature)
                np.savetxt(utt, post)
            else:
                post = model.predict_dense(d)
                np.savetxt(utt, post)


if __name__ == '__main__':
    main()
