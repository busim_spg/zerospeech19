# -*- coding: utf-8 -*-
"""
Created on Sat Sep  1 16:23:40 2018

@author: USER
"""

import random
import numpy as np


def StopEarly(History, patience=5, mode='min', difference=0.001,
              abs_comp=None):
    if mode == 'max':
        h = [-a for a in History]
    else:
        h = History
    History = h
    L = len(History)
    if L <= patience:
        return False
    recent_history = History[L-patience:L]
    antiquity = History[0:L-patience]
    if abs_comp:
        jc = abs_comp
    else:
        jc = min(antiquity)
    ma = min(recent_history)

    if jc - ma >= difference:
        return False
    else:
        return True


def reduce_lr_on_plateau(History, lr, cooldown=0, patience=5,
                         mode='min', difference=0.001,
                         lr_scale=0.5, lr_min=0.00001,
                         cool_down_patience=None,
                         abs_comp=None):
    if cool_down_patience and cooldown <= cool_down_patience:
        return lr, cooldown+1
    assert lr_scale < 1
    if mode == 'max':
        h = [-a for a in History]
    else:
        h = History
    History = h
    L = len(History)
    if L <= patience:
        return lr, cooldown+1
    recent_history = History[L-patience:L]
    antiquity = History[0:L-patience]
    ma = min(recent_history)
    if abs_comp:
        jc = abs_comp
    else:
        jc = min(antiquity)

    if jc - ma >= difference:
        return lr, cooldown+1
    else:
        return max(lr*lr_scale, lr_min), 0


def loadstr(filename, sep=',', dtype=int):
    phrase = open(filename).read().split(sep)
    try:
        diter = iter(dtype)
        assert len(dtype) == len(phrase)
        return [dtype[i](x) for i, x in enumerate(diter)]
    except TypeError:
        return [dtype(x) for x in phrase]


def sample_lr(coeff=[1, 10], exp=[3, 7], k=10, verbose=True):
    population_size = (coeff[1]-coeff[0])*(exp[1]-exp[0])
    sample_indices = random.sample(range(population_size), k)
    coeffs = list(range(coeff[0], coeff[1]))
    exps = list(range(exp[0], exp[1]))
    vals = []
    for index in sample_indices:
        a = index//len(coeffs)
        b = index - a*len(coeffs)
        vals += [(coeffs[b], exps[a])]
    if verbose:
        for val in vals:
            print("{}e-{}".format(val[0], val[1]), end=' ')
    # a = random.choices(range(coeff[0], coeff[1]), k=k)
    # b = random.choices(range(exp[0], exp[1]), k=k)
    # if verbose:
    #     for i, val1 in enumerate(a):
    #         val2 = b[i]
    #         print("{}e-{}".format(val1, val2))
    return vals


def splice(x, indices):
    l_ = x.shape[1]
    X = []
    for i in indices:
        if i < 0:
            x_i = np.pad(x, [[0, 0], [-i, 0]], mode='edge')[:, 0:l_]
        else:
            x_i = np.pad(x, [[0, 0], [0, i]], mode='edge')[:, -l_:]
        X.append(x_i)
    return np.concatenate(X, axis=1)
