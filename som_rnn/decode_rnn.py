#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 20:24:16 2019

@author: yusuf
"""

import argparse
import os
import numpy as np
import torch
import torch.nn.functional as F
from shutil import copy
from torch.utils.data import Dataset, DataLoader
from rnn import forwardRNN, chk_mkdir, get_device


class FeatDataset(Dataset):
    def __init__(self, data,
                 keys,
                 offsets,
                 transform=None):
        self.keys = keys
        self.offsets = offsets
        self.data = data
        self.K = data[0].shape[1]
        self.N = max([_.shape[0] for _ in data])
        self.transform = transform

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        document_mat = self.data[index]
        embed_document = np.zeros((self.N, self.K), dtype='float32')
        embed_document[0:document_mat.shape[0], :] = document_mat

        actual_length = document_mat.shape[0]
        sample = [embed_document, actual_length]
        if self.transform:
            sample = self.transform(sample)
        return sample


def save_texts(mats, filenames, fmt='%.4f', dirname=None, sep=' ', end=''):
    assert len(mats) == len(filenames)
    for i, mat in enumerate(mats):
        name = filenames[i]
        if not name.endswith(end):
            name = name + end
        if dirname:
            name = os.path.join(dirname, name)
        np.savetxt(name, mat, fmt=fmt, delimiter=sep)


def save_mats(mats, dirname, offsets=None, keys=None):
    np.save(os.path.join(dirname, 'data.npy'), mats)
    if offsets:
        copy(offsets, dirname)
    if keys:
        copy(keys, dirname)


def to_categorical(vec_cats, max_val=None):
    if max_val is None:
        max_val = max(vec_cats) + 1
    mat_cats = np.eye(max_val)
    mat_cats = mat_cats[vec_cats]
    return mat_cats.astype(int)


def fn_remove_reps(mat, eps=0):
    output = [mat[0]]
    for x in mat[1:]:
        if abs((x - output[-1])).sum() > eps:
            output.append(x)
    return np.asarray(output, dtype=mat.dtype)


def main():
    parser = argparse.ArgumentParser('Decode the provided features')
    parser.add_argument('datadir', help='The training features directory. '
                        'Should contain: '
                        '    data.npy: A matrix of the features'
                        '    offsets_data.npy: A matrix utterance boundaries'
                        '    keys_data.txt: A list of utterance names')
    parser.add_argument('nnetdir',
                        help='Directory that contains the saved network files')
    parser.add_argument('outdir',
                        help='Directory into which to store the result files')

    parser.add_argument('--batch-size', '-b', default=256, type=int,
                        help='Batch size')
    parser.add_argument('--no-cuda', action='store_true',
                        help='If specified, turns off gpu usage.')
    parser.add_argument('--segments-file', '-s',
                        help='File that maps utterances to documents')
    parser.add_argument('--make-one-hot', action='store_true',
                        help='Make one hot representation')
    parser.add_argument('--remove-reps', action='store_true',
                        help='Remove repetitions')
    parser.add_argument('--save-output-as-npy', action='store_true',
                        help='Store the output as numpy files instead of text')

    args = parser.parse_args()
    datadir = args.datadir
    nnetdir = args.nnetdir
    outdir = args.outdir

    no_cuda = args.no_cuda
    batch_size = args.batch_size
    segments_file = args.segments_file
    save_output_as_npy = args.save_output_as_npy
    make_one_hot = args.make_one_hot
    remove_reps = args.remove_reps

    device = get_device(no_cuda)
    frnndir = os.path.join(nnetdir, 'frnn')
    drnn = forwardRNN.load_from_dir(frnndir, map_location=device)
    drnn.to(device)
    drnn.eval()

    data = np.load(
            os.path.join(datadir, 'data.npy')
            ).astype('float32')
    offs = np.load(
            os.path.join(datadir, 'offsets_data.npy')
            )
    keys_file = os.path.join(datadir, 'keys_data.txt')
    keys = {line.strip(): i for i, line
            in enumerate(open(os.path.join(datadir, 'keys_data.txt')))}
    keys_dict = {x: i for i, x in enumerate(keys)}

    segments = {}
    if segments_file:
        for line in open(segments_file):
            ln = line.strip().split()
            if ln[1] not in segments.keys():
                segments[ln[1]] = []
            segments[ln[1]].append(ln[0])
    else:
        segments = {x: [x] for x in keys}

    data = np.array_split(data, offs)

    dataset = FeatDataset(data, keys, offs)
    dataloader = DataLoader(dataset, batch_size=batch_size,
                            shuffle=False, num_workers=1, pin_memory=True)

    indices = []
    for sample, len_sample in dataloader:
        sample = sample.to(device)
        with torch.set_grad_enabled(False):
            sample_index = drnn.eval_sequence(torch.transpose(sample, 1, 0))
            sample_index = F.softmax(sample_index, dim=-1)
            for i, l in enumerate(len_sample):
                si = sample_index[0:l, i, :]
                indices.append(
                        si.detach().to('cpu').numpy()
                        )
    y = np.concatenate(indices)
    if make_one_hot:
        y = y.argmax(axis=1)
        y = to_categorical(y)
        fmt = "%d"
    else:
        fmt = "%.4f"
    y_split = np.split(y, offs)
    if remove_reps:
        y_split = [fn_remove_reps(a) for a in y_split]
    doc_keys = []
    z_split = []
    for doc in segments.keys():
        doc_keys.append(doc)
        s_ = segments[doc]
        _tmp = []
        for utt in s_:
            if utt not in keys_dict.keys():
                continue
            x = y_split[keys_dict[utt]]
            if len(x.shape) == 1:
                x = x.reshape(1, -1)
            _tmp.append(x)
        _tmp = np.concatenate(_tmp)
        z_split.append(_tmp)

    odir1 = outdir
    chk_mkdir(odir1)
    if len(z_split) != len(doc_keys):
        print("\t\t\tError: The number of feature files does not " +
              "match the number of keys; {}!={}. Skipping".format(
                      len(z_split), len(doc_keys)))
        exit(1)

    if save_output_as_npy:
        save_mats(y, odir1, offs, keys_file)
    else:
        save_texts(z_split, doc_keys, dirname=odir1, end='.txt', fmt=fmt)


if __name__ == '__main__':
    main()
